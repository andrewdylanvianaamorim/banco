using Microsoft.AspNetCore.Mvc;

namespace Banco.Controllers;

[ApiController]
[Route("api")]
public class BancoController : ControllerBase
{
    [HttpGet]
    [Route("teste")]
    public string Teste()
    {
        return "Olá, mundo!";
    }   
}


