create table Cliente(
	Cpf char(11) not null,
    Nome varchar(120),
    primary key(Cpf)
);

create table Conta(
	Id int not null auto_increment,
    Dono char(11) not null,
    Saldo decimal(18,2) not null default '0.0',
    primary key(Id),
    Foreign key(Dono) references Cliente(Cpf) ON DELETE NO ACTION
);


