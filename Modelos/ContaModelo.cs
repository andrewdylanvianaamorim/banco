namespace Banco.Modelos;

public class ContaModelo
{
    public int Id{get;set;}
    public string Dono{get;set;}
    public decimal Saldo{get;set;} 

    public virtual ClienteModelo ClienteDono{get;set;}
}
