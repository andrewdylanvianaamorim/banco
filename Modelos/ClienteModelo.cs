namespace Banco.Modelos;

public class ClienteModelo
{
    public string Cpf{get;set;}
    public string Nome{get;set;}
    // tem que ser virtual para que o entity framework não tente prencher este campo 
    // olhe: https://learn.microsoft.com/en-us/ef/ef6/fundamentals/relationships
    public virtual ICollection<ContaModelo> Contas{get;set;}
   
    public ClienteModelo()
    {
        Contas = new List<ContaModelo>();
    }
}

